FROM composer:1.8

FROM php:7.3-fpm-alpine

# Install apache 2.4.38 and packages for composer
RUN apk add --no-cache \
	apache2-proxy \
	shadow \
	git \
	subversion \
	openssh \
	mercurial \
	tini \
	bash \
	patch \
	make \
	zip \
	unzip
# Install php extensions : gd, zip, mysqli opcache ...
RUN apk add --no-cache --virtual .build-deps \
		libjpeg-turbo-dev \
		libpng-dev \
		libzip-dev \
		zlib-dev \
		$PHPIZE_DEPS \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-configure zip --with-libzip \
	&& docker-php-ext-install -j$(getconf _NPROCESSORS_ONLN) gd mysqli opcache zip \
	&& pecl install -f xdebug \
	&& runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
		| tr ',' '\n' \
		| sort -u \
		| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
		)" \
	&& apk add --virtual .rakija-phpexts-rundeps $runDeps \
	&& apk del .build-deps

# Copy composer from first image and install hirak/prestissimo
COPY --from=0 /usr/bin/composer /usr/bin/composer
RUN composer global require hirak/prestissimo

# Configure apache to run as www-data and send logs to docker
RUN sed -ri \
		-e 's/User apache/User www-data/g' \
		-e 's/Group apache/Group www-data/g' \
		-e 's!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g' \
		-e 's!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g' \
		/etc/apache2/httpd.conf

EXPOSE 80

# ACTIVATE rewrite, proxy, proxy_http, proxy_fcgi, deflate & expires modules.
RUN sed -i \
  -e 's/#LoadModule rewrite_module/LoadModule rewrite_module/g' \
  -e 's/#LoadModule proxy_module/LoadModule proxy_module/g' \
  -e 's/#LoadModule proxy_http_module/LoadModule proxy_http_module/g' \
  -e 's/#LoadModule proxy_fcgi_module/LoadModule proxy_fcgi_module/g' \
  -e 's/#LoadModule deflate_module/LoadModule deflate_module/g' \
  -e 's/#LoadModule expires_module/LoadModule expires_module/g' \
  /etc/apache2/httpd.conf

# Apache & PHP FPM configuration
ENV DOCUMENT_ROOT=/var/www/html \
  FPM_PM=ondemand \
  FPM_PM_MAX_CHILDREN=5 \
  FPM_PM_START_SERVERS=2 \
  FPM_PM_MIN_SPARE_SERVERS=1 \
  FPM_PM_MAX_SPARE_SERVERS=3 \
  FPM_PM_MAX_REQUESTS=500 \
  APACHE_ROBOTS_TXT=true \
  APACHE_AUTH_LOGIN=anonymous \
  APACHE_AUTH_PASSWORD= \
	APACHE_TIMEOUT=600

# Generate robots.txt file to disallow search engine accesses
RUN { \
    echo 'User-agent: *'; \
    echo 'Disallow: /'; \
  } > /etc/apache2/robots.txt

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Install Xdebug in php-xdebug.ini file outside default ini scan dir
RUN { \
		echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)"; \
    echo "xdebug.remote_enable=on"; \
    echo "xdebug.remote_autostart=on"; \
    echo "xdebug.remote_connect_back=on"; \
	} > /usr/local/etc/php-xdebug.ini

# Copy PHP-FPM config file.
COPY config/fpm/* /usr/local/etc/php-fpm.d/all/


# Set default performance configuration
COPY config/apache/performance.conf /etc/apache2/conf.d/performance.conf

# Include our vhost with php-fpm
COPY config/apache/vhost.conf /etc/apache2/conf.d/vhost.conf

# No entrypoint used by this image
ENTRYPOINT []
COPY config/apache-php-fpm /usr/local/bin/
CMD ["apache-php-fpm"]

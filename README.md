# RAKIJA

[![pipeline status](https://gitlab.com/hostinauvergne/rakija/badges/master/pipeline.svg)](https://gitlab.com/hostinauvergne/rakija/commits/master)

Docker image ready for Kubernetes to run Wordpress, Drupal, Symfony and more...

* Production ready. Tested on major CMS and frameworks : Wordpress, Drupal, Symfony ...
* Only 72MB, lightweight image based on [alpine](https://hub.docker.com/_/alpine)
* Highly configurable using env variables

# Description

Rakija contains :
* Apache 2.4 (with FCGI)
* PHP 7.3
* Composer 1.8
* Xdebug 2.7

Because it uses [multi stage builds](https://docs.docker.com/develop/develop-images/multistage-build/), Rakija needs Docker 17.05+.

# Test it

Run rakija to serve current directory on http://localhost
```
docker run -p 80:80 -v $(pwd)/www:/var/www/html hostinauvergne/rakija
```

# Configuration

Using environment variables, Rakija can :
* run PHP as your user to avoid permissions issues
* set a basic http authentication
* block bots from indexing content

You can handle any type of deployment with this single image.

# Available tags

> **Pull command** :
> docker pull [registry.gitlab.com/hostinauvergne/rakija](https://gitlab.com/hostinauvergne/rakija/container_registry)

* apache2.4-php7.3

There is no *latest* image to avoid updating accidentally.

# Documentation

Configuration is done by passing env variables at run command :
```
docker run -e USER_ID=1000 -d hostinauvergne/rakija
```

| variable                 | Description                                                       | Default value |
|--------------------------|-------------------------------------------------------------------|---------------|
| USER_ID                  | www-data will use this id both for apache and php-fpm             | 82            |
| DOCUMENT_ROOT            | Directory root for apache2 and php                                | /var/www/html |
| FPM_PM                   | PHP-FPM pm (ondemand, dynamic or static)                          | ondemand      |
| FPM_PM_MAX_CHILDREN      | PHP-FPM pm.max children                                           | 5             |
| FPM_PM_START_SERVERS     | PHP_FPM pm.start_servers                                          | 2             |
| FPM_PM_MIN_SPARE_SERVERS | PHP_FPM pm.min_spare_servers                                      | 1             |
| FPM_PM_MAX_SPARE_SERVERS | PHP_FPM pm.max_spare_servers                                      | 3             |
| FPM_PM_MAX_REQUESTS      | PHP_FPM pm.max_requests                                           | 500           |
| APACHE_ROBOTS_TXT        | If true, a robots.txt is used to forbid access to search engines. | true          |
| APACHE_AUTH_PASSWORD     | If defined, an authorization is required to access server.        | (empty)       |
| APACHE_AUTH_LOGIN        | Default login used only if a password is defined.                 | anonymous     |
| APACHE_TIMEOUT           | The Apache timeout value in seconds.                              | 600          |

# Xdebug

To use [Xdebug](https://xdebug.org/), you must set a cookie named *XDEBUG_SESSION* 
and listen for Xdebug on port 9000 in your favourite IDE.

# Build image

In current directory :

```
docker build . -t rakija
```